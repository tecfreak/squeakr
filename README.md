# README #

A simple Twitter-like app built using Python - Flask, as part of the final project session of the DevBootcamp @ MJCET.

See: http://squeakr.herokuapp.com

### How do I get set up? ###

* Clone this repository on your local machine using the following command:
* "git clone https://tecfreak@bitbucket.org/tecfreak/squeakr.git"
* Improve the app by adding new functionality
* Deploy it to your own heroku account.
* Configure the database
* and enjoy!

### Who do I talk to? ###

* me@fkhan.in
